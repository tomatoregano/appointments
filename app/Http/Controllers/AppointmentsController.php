<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Query\Builder;
use App\Appointment;

class AppointmentsController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Find appointments by matching words
     */
    public function find($wordsString)
    {
        $separatedWords = preg_split('/\s+/', $wordsString, -1, PREG_SPLIT_NO_EMPTY);

        $result =  Appointment::where(function ($query) use ($separatedWords) {
            foreach ($separatedWords as $word) {
                $query->orWhere('name', 'like', "%$word%");
            }
        })->get();

        return response()->json($result);
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
