<?php

namespace App\Console\Commands;

use App\Http\Controllers\AppointmentsController;
use Illuminate\Console\Command;
use app\utils;

class GetAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointments:get {words*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Appointments that contain all the words inserted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $words = $this->argument('words');

        $this->getResults($words);
    }

    private function getResults($words)
    {
        $this->error('dsadsa');

        $client = new \GuzzleHttp\Client();

        echo 'asdsadas';

        $response = $client->request(
            'GET',
            "http://127.0.0.2/appointment/" . implode(" ", $words)
        );

        $this->info(strval($response->getBody()));
    }
}
