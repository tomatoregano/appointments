<?php

use Illuminate\Database\Seeder;
use App\Appointment;

class AppointmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Appointment::truncate();

        $faker = \Faker\Factory::create();

        Appointment::create([
            'created_at' => $faker->date,
            'status' => 'ended',
            'name'  => 'doctor appointment'
        ]);

        Appointment::create([
            'created_at' => $faker->date,
            'status' => 'in progress',
            'name'  => 'plumber'
        ]);

        Appointment::create([
            'created_at' => $faker->date,
            'status' => 'open',
            'name'  => 'business lunch: Ladia Inc.'
        ]);

        Appointment::create([
            'created_at' => $faker->date,
            'status' => 'open',
            'name'  => 'SCRUM meeting'
        ]);
    }
}
